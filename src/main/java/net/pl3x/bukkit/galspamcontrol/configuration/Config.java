package net.pl3x.bukkit.galspamcontrol.configuration;

import net.pl3x.bukkit.galspamcontrol.GALSpamControl;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static int SPAM_DELAY = 300;

    public static void reload() {
        GALSpamControl plugin = GALSpamControl.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        SPAM_DELAY = config.getInt("spam-delay", 300);
    }
}
