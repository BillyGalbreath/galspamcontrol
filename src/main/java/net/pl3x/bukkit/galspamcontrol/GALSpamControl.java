package net.pl3x.bukkit.galspamcontrol;

import com.swifteh.GAL.RewardEvent;
import net.pl3x.bukkit.galspamcontrol.command.CmdGALSpamControl;
import net.pl3x.bukkit.galspamcontrol.configuration.Config;
import net.pl3x.bukkit.galspamcontrol.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GALSpamControl extends JavaPlugin implements Listener {
    private final Set<String> recentlyVoted = new HashSet<>();

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        Bukkit.getPluginManager().registerEvents(this, this);

        getCommand("galspamcontrol").setExecutor(new CmdGALSpamControl(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        recentlyVoted.clear();

        Logger.info(getName() + " disabled.");
    }

    public static GALSpamControl getPlugin() {
        return GALSpamControl.getPlugin(GALSpamControl.class);
    }

    @EventHandler
    public void onVoteReward(RewardEvent event) {
        String broadcastMessage = event.getBroadcastMessage();
        if (broadcastMessage == null || ChatColor.stripColor(broadcastMessage).isEmpty()) {
            Logger.debug("Nothing to suppress.");
            return;
        }

        Set<String> onlineNames = Bukkit.getOnlinePlayers().stream().map((Function<Player, String>) HumanEntity::getName).collect(Collectors.toSet());
        for (String name : onlineNames) {
            if (!broadcastMessage.contains(name)) {
                continue;
            }
            if (recentlyVoted.contains(name)) {
                Logger.debug("Suppressed vote broadcast for " + name + ": " + broadcastMessage);
                event.setBroadcastMessage(""); // suppress this broadcast
                return;
            }

            // add voter name
            recentlyVoted.add(name);

            // remove voter name after spam delay
            Bukkit.getScheduler().runTaskLater(this,
                    () -> recentlyVoted.remove(name),
                    Config.SPAM_DELAY * 20);
        }
    }
}
